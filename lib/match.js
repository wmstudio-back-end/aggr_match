
// ----------------------------------- E1 ---------------------------------------
function e1(objectResume, objectVacancy) {
    //console.log("Fe1: "+objectVacancy.match.e1)
    if (objectVacancy.match.e1 === 0) {
        return false;
    }
    if(objectVacancy.match.e1 === 1){
        if (objectVacancy.minEducation<=objectResume.currentLevelOfEducation-1 &&
            objectResume.languageSkills.length>0){
                return true;
        }
    }
    if(objectVacancy.match.e1 === 2){
            if (objectVacancy.minEducation<=objectResume.currentLevelOfEducation &&
                LanguagesRequiredLang(objectVacancy.LanguagesRequired, objectResume.languageSkills, 1)){
                return true;
            }
    }
    if(objectVacancy.match.e1 === 3){
            if (objectVacancy.minEducation<=objectResume.currentLevelOfEducation &&
                LanguagesRequiredLang(objectVacancy.LanguagesRequired, objectResume.languageSkills, 0)){
                return true;
            }
    }
    return false;
}

function LanguagesRequiredLang(languagesRequired, languageSkills, level = 0) {
    var ret = false;
    languagesRequired.forEach(function (objectLanguagesRequired) {
        languageSkills.forEach(function (objectLanguageSkills) {
            if (objectLanguagesRequired.languageID===objectLanguageSkills.langId &&
                objectLanguagesRequired.levelID<=objectLanguageSkills.langLevel-level){
                ret = true;
            }
        })
    })
    return ret;
}

// ----------------------------------- E2 -----------------------------------
function e2(objectResume, objectVacancy) {
    //console.log("Fe2: "+objectVacancy.match.e2)
    //console.log(user.resume_docs);
    //console.log(user.resume_docs.workExp);
    if (objectVacancy.match.e2 === 0) {
        return false;
    }

    if (objectVacancy.match.e2 === 1) {
        return true;
    }
    //console.log(user.resume_docs.workExp);

    if (objectVacancy.match.e2 === 2 &&
        objectVacancy.experience_work>0 &&
        objectResume.workExp.length>0){
        return true;
    }
    //console.log(objectResume.internshipOptions)
    if (objectVacancy.match.e2 === 3 &&
        (((objectVacancy.type === 0 ||
            objectVacancy.type == 1) &&
            occupationField(objectVacancy.occupationField, objectResume.internshipOptions)) ||//??
            //objectVacancy.occupationField == user.resume_docs.internshipOptions.occupationField) ||???

        (objectVacancy.type==2 ||
            objectVacancy.type==3 ||
            objectVacancy.type==4) &&
                occupationField(objectVacancy.occupationField, objectResume.jobOptions) ||//??
            //objectVacancy.occupationField==user.resume_docs.jobOptions.occupationField) &&

        objectVacancy.experience_work>0 && objectResume.workExp.length>0 && objectVacancy.experience_work>0 && objectResume.workExp.length>0 &&
            occupationField(objectVacancy.occupationField, objectResume.workExp))
        //db.resume.workExp.$.occupationField == objectVacancy.occupationField) ???
        ){
        return true;
    }
    return false;
}
/*
function workExpLengthBoolean(resume_docs) {
    var ret = false;
    resume_docs.forEach(function (object) {
        if(object.workExp.length>0){ret = true}
    })
    return ret;
}
*/

function occupationField(VacancyOccupationField, objectResumeInternshipOptions) {
    var ret = false;
/*
    console.log(objectResumeInternshipOptions)
    console.log("------------------")
*/
    objectResumeInternshipOptions.occupationField.forEach(function (object) {
        if (VacancyOccupationField === object) { ret = true; return 0;}
    })
    return ret;
}

function e3(objectResume, objectVacancy) {
    //console.log("Fe3: "+objectVacancy.match.e3);
    if (objectVacancy.match.e3 === 0) {
        return false;
    }

    if (objectVacancy.match.e3 === 1 &&
        (arrInArrAsBoolean(objectVacancy.keySkills, objectResume.pickedSkills, 1) ||
        arrInArrAsBoolean(objectVacancy.Desired_personality_traits, objectResume.strenghs, 1) ||
            arrInArrAsBoolean(objectVacancy.Desired_personality_traits, objectResume.characterTraits,1 )) ){
        return true;
    }
    if (objectVacancy.match.e2 === 2 &&
        (arrInArrAsBoolean(objectVacancy.keySkills, objectResume.pickedSkills, 1) &&
            (arrInArrAsBoolean(objectVacancy.Desired_personality_traits, objectResume.strenghs, 1) ||
            arrInArrAsBoolean(objectVacancy.Desired_personality_traits, objectResume.characterTraits,1 )) )){
        return true;
    }
    if (objectVacancy.match.e2 === 3 &&
        (arrInArrAsBoolean(objectVacancy.keySkills, objectResume.pickedSkills, 2) &&
            (arrInArrAsBoolean(objectVacancy.Desired_personality_traits, objectResume.strenghs, 1) ||
                arrInArrAsBoolean(objectVacancy.Desired_personality_traits, objectResume.characterTraits,1)) )){
        return true;
    }

    return false;
}

function arrInArrAsBoolean(a, b, count = 1) {
/*
    console.log("a: ")
    console.log(a)
    console.log("b: ")
    console.log(b)
*/
    var i = 0;
    a.forEach(function (objectA) {
        b.forEach(function (objectB) {
            if(objectA.id === objectB.id){
                i++;
            }
        })
    })
    //console.log(i);
    if(count<=i){
        return true;
    }else{
        return false;
    }
}
// --------------------------------------------------------------------------

function matrix(school_user_datas, occupationField, matchE2){
    var matrix_value = [
        [1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	2,	1,	1,	1,	1,	1,	3,	1,	1,	3],
        [2,	3,	1,	2,	2,	3,	3,	3,	2,	1,	1,	1,	1,	2,	2,	3,	1,	1,	1,	1,	2,	3],
        [2,	2,	1,	2,	1,	2,	2,	2,	1,	1,	1,	1,	1,	3,	1,	2,	1,	1,	1,	2,	2,	3],
        [1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	3,	1,	1,	1,	3,	1,	1,	1,	1,	1,	1,	3],
        [1,	1,	1,	1,	1,	2,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	3,	1,	3],
        [1,	1,	2,	1,	1,	1,	1,	1,	1,	2,	3,	3,	2,	1,	2,	1,	1,	1,	2,	1,	1,	3],
        [1,	1,	1,	1,	3,	1,	1,	2,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	3],
        [1,	1,	3,	1,	1,	1,	1,	1,	1,	3,	2,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	3],
        [1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	2,	1,	1,	2,	1,	1,	1,	3,	1,	1,	3],
        [1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	3,	1,	1,	1,	1,	3],
        [1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	3,	1,	2,	1,	3],
        [2,	1,	1,	2,	1,	1,	1,	1,	1,	1,	1,	1,	1,	2,	1,	2,	1,	2,	1,	2,	1,	3],
        [1,	1,	1,	1,	1,	1,	2,	2,	1,	1,	1,	1,	1,	2,	2,	3,	1,	1,	1,	1,	3,	3],
        [1,	1,	1,	1,	1,	1,	1,	1,	3,	1,	1,	1,	2,	1,	1,	1,	1,	1,	1,	1,	1,	3],
        [1,	1,	2,	1,	2,	1,	1,	1,	1,	2,	1,	1,	1,	1,	1,	1,	1,	2,	1,	1,	1,	3],
        [2,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	2,	1,	1,	1,	1,	1,	3],
        [1,	1,	1,	1,	1,	1,	3,	1,	1,	1,	1,	1,	1,	1,	1,	2,	1,	3,	1,	1,	1,	3],
        [1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	2,	2,	3,	1,	1,	1,	1,	1,	2,	1,	1,	3],
        [2,	2,	1,	2,	2,	1,	2,	2,	1,	1,	1,	1,	1,	2,	1,	3,	2,	2,	1,	1,	2,	3],
        [3,	2,	1,	3,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	2,	1,	1,	2,	1,	3],
        [1,	1,	3,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	1,	3]
        ];
    var ret = false;
    var val;
/*
    console.log(school_user_datas)
    console.log("occupationField"+occupationField)
    console.log("E2: "+matchE2)
*/
    school_user_datas.forEach(function (object) {
        if(object.major){
/*
            console.log("object.major"+object.major)
            console.log("matrix: "+matrix_value[object.major][occupationField])
*/
            val = object.major;
        }
        if(object.minor && !val){
/*
            console.log("object.minor"+object.minor)
            console.log("matrix: "+matrix_value[object.minor][occupationField])
*/
            val = object.minor;
        }
        if(val){
            if (matrix_value[val][occupationField] === 1){
                ret = true;
            }
            if ((matrix_value[val][occupationField] === 2) &&
                (matchE2 === 2 || matchE2 === 3)){
                ret = true;
            }
            if ((matrix_value[val][occupationField] === 3) && matchE2 === 3){
                ret = true;
            }
        }
    })
    //console.log(school_user_datas)
    return ret;
}
exports.eN = function (user) {
    var ret = [];
    var time = parseInt(new Date().getTime() / 1000);
    user.vacancy_docs.forEach(function (objectVacancy) {
        user.resume_docs.forEach(function (objectResume) {

            /*console.log(objectVacancy.match);
            console.log("e1: "+e1(objectResume, objectVacancy));
            console.log("e2: "+e2(objectResume, objectVacancy));
            console.log("e3: "+e3(objectResume, objectVacancy));
            console.log("matrix: "+matrix(user.school_user_data_docs, objectVacancy.occupationField, objectVacancy.match.e2));*/
            if (e1(objectResume, objectVacancy) &&
                e2(objectResume, objectVacancy) &&
                e3(objectResume, objectVacancy) &&
                matrix(user.school_user_data_docs, objectVacancy.occupationField, objectVacancy.match.e2)){
                ret.push({
                    "resumeId": objectResume._id,
                    "vacancyId": objectVacancy._id,
                    //"created_at": time
                });
            }

        })
    });
    //console.log("registration("+error_code[user.type_profile]+"): "+user._id);
    return ret;
}

