db.getCollection('users').aggregate([
        {$lookup: {
                from: "packages",
                localField: "service.active.packageId",
                foreignField: "_id",
                as: "packages_docs",
            }},
        {
            $match: {
                "packages_docs": {$ne: []},
                "packages_docs.settings.match": true
            }},
        {$lookup: {
                from: "user_order",
                localField: "service.active.orderId",
                foreignField: "_id",
                as: "user_order_docs"
            }},
        {
            $match: {
                "user_order_docs": {$ne: []},
                "user_order_docs.status": 1
            }},
        {$lookup: {
                from: "school_user_data",
                localField: "_id",
                foreignField: "uid",
                as: "school_user_data_docs"
            }},
        {
            $match: {
                "school_user_data_docs": {$ne: []},
                //"service_docs.status": 1
            }},
        {$lookup: {
                from: "resume",
                localField: "_id",
                foreignField: "uid",
                as: "resume_docs"
            }},
        {
            $match: {
                "resume_docs": {$ne: []},
                "resume_docs.workExp.currentlyWorking": false,
                //"service_docs.status": 1
            }},
        {$lookup: {
                from: "vacancy",
                localField: "Student.ready_to_work",
                foreignField: "match.cand",
                as: "vacancy_docs"
            }},
        {
            $match: {
                "vacancy_docs": {$ne: []},
                "vacancy_docs.match.work": 1,
                //"service_docs.status": 1
            }}
    // не надо тянуть с базы кучу не нужных данных
    ,{$project: {"_id": 1,
                "resume_docs._id": 1,
                "resume_docs.workExp.currentlyWorking": 1,
                "resume_docs.currentLevelOfEducation": 1,
                "resume_docs.languageSkills.langId": 1,
                "resume_docs.languageSkills.langLevel": 1,
                "resume_docs.workExp.occupationField": 1,
                "resume_docs.jobOptions.occupationField": 1,
                "resume_docs.internshipOptions.occupationField": 1,
                "resume_docs.pickedSkills": 1,
                "resume_docs.characterTraits": 1,
                "resume_docs.strenghs": 1,

                "vacancy_docs._id": 1,
                "vacancy_docs.match": 1,
                "vacancy_docs.keySkills": 1,
                "vacancy_docs.Desired_personality_traits": 1,
                "vacancy_docs.experience_work": 1,
                "vacancy_docs.occupationField": 1,
                "vacancy_docs.type": 1,
                "vacancy_docs.minEducation": 1,
                "vacancy_docs.LanguagesRequired.levelID": 1,
                "vacancy_docs.LanguagesRequired.languageID": 1
            }}
    ])