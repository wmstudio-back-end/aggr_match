
argv = require('minimist')(process.argv.slice(2));
console.log(argv)

/*
 Seconds: 0-59
 Minutes: 0-59
 Hours: 0-23
 Day of Month: 1-31
 Months: 0-11
 Day of Week: 0-6
 * */
var CronJob = require('cron').CronJob;
var job = new CronJob({
    cronTime: '* * 12 * * *',
    onTick: startMatch,
    start: false,
    timeZone: 'America/Los_Angeles'
});
 job.start();


var mongoClient = require("mongodb").MongoClient;
var ObjectId = require('mongodb').ObjectID;

//var notification = require('./lib/notification.js');
//var patch = require('./lib/patch.js');
//var test = require('./lib/test.js');
var match = require('./lib/match.js');


var url = "mongodb://project:project@localhost:27017/project";
function startMatch(){


mongoClient.connect(url, function(err, db) {
    if (err) return console.log(err);

    // clearTimeout(timeoutQueue);
    // timeoutQueue = setTimeout(hung, 3000);

    var time = parseInt(new Date().getTime() / 1000);
    db.collection("users").aggregate([
        {$lookup: {
                from: "packages",
                localField: "service.active.packageId",
                foreignField: "_id",
                as: "packages_docs",
            }},
        {
            $match: {
                "packages_docs": {$ne: []},
                "packages_docs.settings.match": true
            }},
        {$lookup: {
                from: "user_order",
                localField: "service.active.orderId",
                foreignField: "_id",
                as: "service_docs"
            }},
        {
            $match: {
                "service_docs": {$ne: []},
                "service_docs.status": 1
            }},
        {$lookup: {
                from: "school_user_data",
                localField: "_id",
                foreignField: "uid",
                as: "school_user_data_docs"
            }},
        {
            $match: {
                "school_user_data_docs": {$ne: []},
                //"service_docs.status": 1
            }},
        {$lookup: {
                from: "resume",
                localField: "_id",
                foreignField: "uid",
                as: "resume_docs"
            }},
        {
            $match: {
                "resume_docs": {$ne: []},
                //"service_docs.status": 1
            }},
        {$lookup: {
                from: "vacancy",
                localField: "Student.ready_to_work",
                foreignField: "match.cand",
                as: "vacancy_docs"
            }},
        {
            $match: {
                "vacancy_docs": {$ne: []},
                //"service_docs.status": 1
            }}
        // не надо тянуть с базы кучу ненужных данных
        ,{$project: {"_id": 1,
                "resume_docs._id": 1,
                "resume_docs.workExp.currentlyWorking": 1,
                "resume_docs.currentLevelOfEducation": 1,
                "resume_docs.languageSkills.langId": 1,
                "resume_docs.languageSkills.langLevel": 1,
                "resume_docs.workExp.occupationField": 1,
                "resume_docs.jobOptions.occupationField": 1,
                "resume_docs.internshipOptions.occupationField": 1,
                "resume_docs.pickedSkills": 1,
                "resume_docs.characterTraits": 1,
                "resume_docs.strenghs": 1,

                "vacancy_docs._id": 1,
                "vacancy_docs.match": 1,
                "vacancy_docs.keySkills": 1,
                "vacancy_docs.Desired_personality_traits": 1,
                "vacancy_docs.experience_work": 1,
                "vacancy_docs.occupationField": 1,
                "vacancy_docs.type": 1,
                "vacancy_docs.minEducation": 1,
                "vacancy_docs.LanguagesRequired.levelID": 1,
                "vacancy_docs.LanguagesRequired.languageID": 1,

                "school_user_data_docs.major": 1,
                "school_user_data_docs.minor": 1,
            }}
     ]
    //     ,function(err,data){
    //     console.log('ssssssssssss',err,data)
    // }
    )
.forEach(function (result) {
        console.log(match.eN(result));
        var time = parseInt(new Date().getTime() / 1000);
        match.eN(result).forEach(function (object) {
            object.created_at = time;
            console.log(object);
            db.collection("match").find({"resumeId": object.resumeId,
                "vacancyId": object.vacancyId}).count(function (err, count) {
                if(count === 0){
                    db.collection("match").insertMany([object]);
                    //console.log("insert")
                }else{
                    //console.log("NOinsert")
                }
            })

        })
/*
        result.vacancy_docs.forEach(function (vacancy_doc) {
            console.log(vacancy_doc.match);
        });
*/
        //console.log(result.vacancy_docs);
        //notification.accountVerification(db, results);
    });

    console.log('END')
    // end connect
});

}
